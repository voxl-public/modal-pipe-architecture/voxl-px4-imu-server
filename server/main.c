/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *	this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *	this list of conditions and the following disclaimer in the documentation
 *	and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *	may be used to endorse or promote products derived from this software
 *	without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *	ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include <modal_start_stop.h>
#include <modal_pipe_server.h>
#include "timestamp.h"

#define PROCESS_NAME	"voxl-px4-imu-server"
#define PIPE_NAME		("imu_px4")
#define PIPE_DIR		(MODAL_PIPE_DEFAULT_BASE_DIR PIPE_NAME)

#define FROM_PX4_PIPE	"/dev/imu-pipe0"
#define ODR 1000 // 1khz from px4



// Copied from another file. Needs to stay the same.
typedef struct icm4x6xx_imu_data_t {
	float accl_ms2[3];	///< XYZ acceleration in m/s^2
	float gyro_rad[3];	///< XYZ gyro rotation in rad/s
	float temp_c;		///< temperature in degrees Celcius
	uint64_t timestamp_monotonic_ns; ///< Monotonic timestamp
	uint64_t dummy0;	///< Not used for VIO
	uint64_t dummy1;	///< Not used for VIO
}__attribute__((packed)) icm4x6xx_imu_data_t;



#define BUF_ITEMS	4096
#define PX4_BUF_LEN	(BUF_ITEMS * sizeof(icm4x6xx_imu_data_t))
#define MPA_BUF_LEN	(BUF_ITEMS * sizeof(imu_data_t))

// local vars
static int en_debug;
static icm4x6xx_imu_data_t px4_data[BUF_ITEMS];
static imu_data_t mpa_data[BUF_ITEMS];






// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
voxl-px4-imu-server usually runs as a systemd background service.\n\
However, for debug purposes it can be started from the command line manually.\n\
When started from the command line, the background service will\n\
stop automatically so you don't have to stop it manually.\n\
\n\
-h, --help                  print this help message\n\
-d, --debug                 run in debug mode\n\
\n");
	return;
}


static void _connect_handler(int ch, int client_id, char* string, __attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to channel %d  with client id %d\n", string, ch, client_id);
	return;
}


static void _disconnect_handler(int ch, int client_id, char* name, __attribute__((unused)) void* context)
{
	printf("client \"%s\" with id %d has disconnected from channel %d\n", name, client_id, ch);
	return;
}

static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",				no_argument,	0,	'd'},
		{"help",				no_argument,	0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "dh", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'h':
			_print_usage();
			return -1;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

////////////////////////////////////////////////////////////////////////////////
// do any setup and start threads HERE
////////////////////////////////////////////////////////////////////////////////

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

	// open pipe
	pipe_info_t info;
	strcpy(info.name, PIPE_NAME);
	strcpy(info.type, "imu_data_t");
	strcpy(info.server_name, PROCESS_NAME);
	info.size_bytes = IMU_RECOMMENDED_PIPE_SIZE;
	int flags = 0;
	if(pipe_server_create(0, info, flags)) return -1;

	pipe_server_set_connect_cb(0, &_connect_handler, NULL);
	pipe_server_set_disconnect_cb(0, &_disconnect_handler, NULL);





	main_running = 1; // this is an extern variable in start_stop.c
	printf("Init complete, entering main loop\n");


	int connected = 0;
	int fd;
	double clock_ratio = 1.0;
	int64_t previous_ts_ns=0;

	while(main_running){

		usleep(5000);

		// try to connect if not connected
		if(!connected){

			// // if not in debug mode, don't bother connecting until we have a client
			// if(pipe_server_get_num_clients(0) == 0 && !en_debug){
			// 	continue;
			// }

			// we have a client and not in debug mode,
			fd = open(FROM_PX4_PIPE, O_RDONLY|O_NONBLOCK);
			if(fd != -1){
				printf("Connected to px4\n");
				connected = 1;
			}
		}

		// if connected, read in data
		if(connected){
			// reads metadata from fifo
			int bytes_read = read(fd, px4_data, PX4_BUF_LEN);

			// if errno is EAGAIN that's okay, just means no data is available
			if(bytes_read<0 && errno==EAGAIN){
				continue;
			}

			if(bytes_read<0){
				fprintf(stderr, "read returned %d\n", bytes_read);
				perror("");
				fprintf(stderr, "Disconnected from px4\n");
				connected = 0;
				close(fd);
				fd = 0;
				continue;
			}

			if(bytes_read == 0){
				continue;
			}

			if(bytes_read%sizeof(icm4x6xx_imu_data_t)){
				fprintf(stderr, "ERROR validating data received through pipe: read partial packet\n");
				fprintf(stderr, "read %d bytes, but it should be a multiple of %zu\n", bytes_read, sizeof(icm4x6xx_imu_data_t));
				continue;
			}

			int packets = bytes_read/sizeof(icm4x6xx_imu_data_t);

			if(en_debug){
				printf("read %d packets\n", packets);
			}

			static int last_read_was_good = 0;

			int64_t most_recent_time = px4_data[packets-1].timestamp_monotonic_ns;
			int64_t filtered_ts_ns = calc_filtered_ts_ns(most_recent_time, packets, &clock_ratio,\
							previous_ts_ns, ODR, last_read_was_good, en_debug);


			int64_t new_dt;
			if(last_read_was_good){
				new_dt = (filtered_ts_ns - previous_ts_ns)/packets;
			}
			else{
				new_dt = 1000000000.0/(ODR/clock_ratio);
			}

			previous_ts_ns = filtered_ts_ns;
			last_read_was_good = 1;


			for(int i=0; i<packets; i++){
				mpa_data[i].magic_number = IMU_MAGIC_NUMBER;
				mpa_data[i].accl_ms2[0] = px4_data[i].accl_ms2[0];
				mpa_data[i].accl_ms2[1] = px4_data[i].accl_ms2[1];
				mpa_data[i].accl_ms2[2] = px4_data[i].accl_ms2[2];
				mpa_data[i].gyro_rad[0] = px4_data[i].gyro_rad[0];
				mpa_data[i].gyro_rad[1] = px4_data[i].gyro_rad[1];
				mpa_data[i].gyro_rad[2] = px4_data[i].gyro_rad[2];
				mpa_data[i].temp_c		= px4_data[i].temp_c;
				//mpa_data[i].timestamp_ns = px4_data[i].timestamp_monotonic_ns;
				mpa_data[i].timestamp_ns = filtered_ts_ns - ((packets-i-1)*new_dt);
			}

			pipe_server_write(0, mpa_data, packets*sizeof(imu_data_t));
		}
	} // end of while()

////////////////////////////////////////////////////////////////////////////////
// Stop all the threads and do cleanup HERE
////////////////////////////////////////////////////////////////////////////////

	printf("Starting shutdown sequence\n");
	pipe_server_close_all();
	if(fd>0) close(fd);
	remove_pid_file(PROCESS_NAME);
	printf("exiting cleanly\n");
	return 0;
}
